php-phpoption (1.9.3-1) unstable; urgency=medium

  * Team upload
  * New upstream version 1.9.3

 -- David Prévot <taffit@debian.org>  Sat, 25 Jan 2025 11:20:27 +0100

php-phpoption (1.9.2-1) unstable; urgency=medium

  * New upstream version 1.9.2
  * Update Standards-Version
  * Mark package as Multi-Arch: foreign
  * Use my debian.org email in Uploaders

 -- Robin Gustafsson <rgson@debian.org>  Wed, 03 Jul 2024 20:38:05 +0200

php-phpoption (1.9.1-1) unstable; urgency=medium

  * New upstream version 1.9.1
  * Support PHPUnit 10 (Closes: #1039816)
  * Update standards version to 4.6.2, no changes needed.

 -- Robin Gustafsson <robin@rgson.se>  Sat, 22 Jul 2023 23:29:28 +0200

php-phpoption (1.8.1-1) unstable; urgency=medium

  * Team upload

  [ Robin Gustafsson ]
  * Set upstream metadata fields: Security-Contact.
  * Bump Standards-Version
  * Replace git attributes with uscan's gitexport=all
  * Rename main branch to debian/latest (DEP-14)
  * Mark test dependencies with build profile spec

  [ David Prévot ]
  * Install /u/s/p/autoloaders file
  * New upstream version 1.8.1
  * Update standards version to 4.6.0, no changes needed.

 -- David Prévot <taffit@debian.org>  Wed, 29 Dec 2021 10:45:11 -0400

php-phpoption (1.7.5-2) unstable; urgency=medium

  * Source only upload for migration to testing
  * Remove Salsa CI config

 -- Robin Gustafsson <robin@rgson.se>  Sun, 06 Dec 2020 16:20:15 +0100

php-phpoption (1.7.5-1) unstable; urgency=medium

  * New upstream release
  * Add Upstream-Name and Upstream-Contact
  * Drop obsolete patches

 -- Robin Gustafsson <robin@rgson.se>  Thu, 27 Aug 2020 20:17:37 +0200

php-phpoption (1.7.3-1) unstable; urgency=medium

  * Initial release (Closes: #951164)

 -- Robin Gustafsson <robin@rgson.se>  Tue, 12 May 2020 20:50:48 +0200
